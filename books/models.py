from django.db import models

# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=100)
    year_published = models.SmallIntegerField()
    rating = models.PositiveSmallIntegerField()
    publisher_name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.title
