from django.shortcuts import render
from .models import Book

# request is initiated, we get the data, smoosh it
# together with html and context, and return it to the browser
def books_list(request):
    list_of_books = Book.objects.all()
    context = {
        'books': list_of_books,
        'heading': 'My list of books is lit:'
    }
    # books/books_list.html is only the path
    return render(request, 'books/book_list.html', context)
